﻿namespace Antick.Messaging
{
    public interface IResponder
    {
        void Subscribe(); 
    }
}