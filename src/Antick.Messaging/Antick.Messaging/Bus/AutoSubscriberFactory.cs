﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Castle.Windsor;
using EasyNetQ;
using EasyNetQ.AutoSubscribe;

namespace Antick.Messaging.Bus
{
    public class AutoSubscriberFactory
    {
        /// <summary>
        /// Создание базового авто-потребителя с автоматическим удалением очередней
        /// </summary>
        /// <param name="container">Контейнер виндзора</param>
        /// <param name="appName">Название приложения, он же ключ, будет использоват в составлении имени очереди</param>
        public static AutoSubscriber BasicAutoDelete(IWindsorContainer container, string appName)
        {
            var autoSubscriber = new AutoSubscriber(container.Resolve<IBus>(), appName)
            {
                AutoSubscriberMessageDispatcher = new WindsorMessageDispatcher(container, container.Resolve<ILogger>()),
                ConfigureSubscriptionConfiguration = c => c.WithAutoDelete(),
                GenerateSubscriptionId = info => appName
            };
            return autoSubscriber;
        }
    }
}
