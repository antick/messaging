﻿using Castle.Core.Logging;
using EasyNetQ;

namespace Antick.Messaging.Bus
{
    public class BusFactory
    {
        private BusConfig _config;
        private ILogger _logger;

        public BusFactory(BusConfig config, ILogger logger)
        {
            _config = config;
            _logger = logger;
        }

        public IBus Create()
        {
            _logger.Debug("Creating connection to RabbitMq...");

            var result = RabbitHutch.CreateBus(_config.ConnectionString);

            _logger.Debug("Connected to RabbitMq successfull");

            return result;
        }
    }
}