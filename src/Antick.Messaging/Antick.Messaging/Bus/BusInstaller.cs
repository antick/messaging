﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EasyNetQ;
using Inceptum.AppServer.Configuration;

namespace Antick.Messaging.Bus
{
    public class BusInstaller : IWindsorInstaller
    {
        private readonly string m_BundleName;
        private readonly string m_JsonPath;

        public BusInstaller(string bundleName, string jsonPath)
        {
            m_BundleName = bundleName;
            m_JsonPath = jsonPath;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<BusConfig>()
                    .FromConfiguration(m_BundleName, m_JsonPath, "{environment}", "{machineName}"),
                Component.For<BusFactory>(),
                Component.For<IBus>().UsingFactoryMethod(kernel => kernel.Resolve<BusFactory>().Create())
                    .LifestyleSingleton()
            );
        }
    }
}