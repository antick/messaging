﻿using System.Reflection;
using Castle.Core;
using Castle.Core.Logging;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using EasyNetQ;
using EasyNetQ.AutoSubscribe;

namespace Antick.Messaging
{
    public class AutoSubscriberStartable : IStartable
    {
        private readonly IWindsorContainer _container;
        private readonly IBus _bus;

        private ILogger _logger;

        private string _subscriptionId;
        
        public AutoSubscriberStartable(IWindsorContainer container, ILogger logger, IBus bus, string subscriptionId)
        {
            _logger = logger;
            _container = container;
            _bus = bus;
            _subscriptionId = subscriptionId;
        }

        public void Start()
        {
            _container.Register(Classes.FromThisAssembly().BasedOn(typeof(IConsume<>)).WithServiceSelf());


            var autoSubscriber = new AutoSubscriber(_bus, _subscriptionId)
            {
                AutoSubscriberMessageDispatcher = new WindsorMessageDispatcher(_container, _logger)
            };

            autoSubscriber.Subscribe(Assembly.GetExecutingAssembly());
        }

        public void Stop()
        {

        }
    }
}
