﻿using System;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using EasyNetQ.AutoSubscribe;

namespace Antick.Messaging
{
    public class WindsorMessageDispatcher : IAutoSubscriberMessageDispatcher
    {
        private readonly IWindsorContainer _container;

        private readonly ILogger _logger;

        public WindsorMessageDispatcher(IWindsorContainer container, ILogger logger)
        {
            this._container = container;
            _logger = logger;
        }

        public void Dispatch<TMessage, TConsumer>(TMessage message)
            where TMessage : class
            where TConsumer : IConsume<TMessage>
        {
            using (_container.BeginScope()) //this ensures data is persisted once the consume is complete
            {
                IConsume<TMessage> consumer = null;
                try
                {
                    consumer = (IConsume<TMessage>)_container.Resolve<TConsumer>();
                    consumer.Consume(message);
                }
                catch (Exception e)
                {
                    _logger.Error("Comsume Failed", e);
                }

                finally
                {
                    _container.Release(consumer);
                }
            }
        }

        public Task DispatchAsync<TMessage, TConsumer>(TMessage message) where TMessage : class where TConsumer : IConsumeAsync<TMessage>
        {
            return Task.Factory.StartNew(() =>
            {
                using (_container.BeginScope()) //this ensures data is persisted once the consume is complete
                {
                    IConsume<TMessage> consumer = null;
                    try
                    {
                        consumer = (IConsume<TMessage>)_container.Resolve<TConsumer>();
                        consumer.Consume(message);
                    }
                    catch (Exception e)
                    {
                        _logger.Error("Comsume Failed", e);
                    }

                    finally
                    {
                        _container.Release(consumer);
                    }
                }
            });
            //.ContinueWith(task =>
            //{
            //    if (task.IsCompleted && !task.IsFaulted)
            //    {
            //        // Everything worked out ok
            //    }
            //    else
            //    {
            //        // Dont catch this, it is caught further up the heirarchy and results in being sent to the default error queue
            //        // on the broker
            //        throw new EasyNetQException("Message processing exception - look in the default error queue (broker)");
            //    }
            //})); ;

        }
    }

}